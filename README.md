js command line script, generates location data (trial bounds and plot bounds), 
from GPS point data, for use in 2021 Serenity trial 'New Edge Microbiols' - 
'Alleviation of water-stress effects on wheat growth by endophytes'.  
  
Input data (./data/plotEndPointsGeoJSON) is array of GeoJSON Point Features
representing the end point of tractor runs, 30m apart. These points correspond
to the middle of the ends of 30m long by 2.1m wide plots.  
Runs 2 to 22 have their first 4m separately sectioned, resulting in a 4m plot
and a 26m plot for these runs.  
  

```  
npm install
node main [ToS1BoundsOutfile] [ToS2BoundsOutfile] [PlotBoundsOutFile]
```
  
ToSXBoundsOutfile => geoJSON of trial boundary polygons.  
PlotBoundsOutFile => csv trial design file, with row, section, and plot geoJSON polygons.  
Prints to screen if arguments not present.  
