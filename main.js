const fs = require('fs');
const generateGeomData = require('./script');

const main = () => {
  const args = process.argv.slice(2);

  // Parse point data to produce trial bounds and per-plot data
  const [trial1Bounds, trial2Bounds, plotCsvStr] = generateGeomData();

  // Write results
  if (args[0]) {
    fs.writeFileSync(args[0], JSON.stringify(trial1Bounds, null, 1));
    console.log('## ToS1 trial bounds saved to ' + args[0]);
  } else {
    console.log('## ToS1 trial bounds ##');
    console.log(JSON.stringify(trial1Bounds, null, 1));
  }
  if (args[1]) {
    fs.writeFileSync(args[1], JSON.stringify(trial2Bounds, null, 1));
    console.log('## ToS2 trial bounds saved to ' + args[1]);
  } else {
    console.log('## ToS2 trial bounds ##');
    console.log(JSON.stringify(trial2Bounds, null, 1));
  }
  if (args[2]) {
    fs.writeFileSync(args[2], plotCsvStr);
    console.log('## Plot csv data saved to ' + args[2]);
  } else {
    console.log('## Plot bounds ##');
    console.log(plotCsvStr);
  }
};

main();
