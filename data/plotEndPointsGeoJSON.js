const plotEndPoints = [
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:42:56',
      Version: 10.13,
      Id: 905,
      Name: '1S',
      Latitude: -34.46983974,
      Longitude: 148.69585902,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332105ddb65dd'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69585902222244,
        -34.46983973930835
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:43:30',
      Version: 10.13,
      Id: 905,
      Name: '1N',
      Latitude: -34.46959166,
      Longitude: 148.69598876,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332326fb187c4'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69598876227212,
        -34.46959165554734
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:46:51',
      Version: 10.13,
      Id: 905,
      Name: '3S',
      Latitude: -34.46985538,
      Longitude: 148.69590254,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332fb0e46c803'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69590253549305,
        -34.46985538157817
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:47:25',
      Version: 10.13,
      Id: 905,
      Name: '3N',
      Latitude: -34.46960746,
      Longitude: 148.69603291,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43331d54eeb418'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6960329085604,
        -34.46960745755854
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:49:25',
      Version: 10.13,
      Id: 905,
      Name: '4S',
      Latitude: -34.46986313,
      Longitude: 148.69592465,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433395338c4ae8'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6959246463705,
        -34.46986313309887
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:49:59',
      Version: 10.13,
      Id: 905,
      Name: '4N',
      Latitude: -34.46961534,
      Longitude: 148.69605487,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4333b76b824efe'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6960548663671,
        -34.46961534117863
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:52:06',
      Version: 10.13,
      Id: 905,
      Name: '6S',
      Latitude: -34.46987893,
      Longitude: 148.69596841,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: 43343656231355
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69596840783342,
        -34.46987893441534
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:52:40',
      Version: 10.13,
      Id: 905,
      Name: '6N',
      Latitude: -34.46963107,
      Longitude: 148.69609884,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43345864dabe79'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69609883641016,
        -34.46963106629145
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:55:27',
      Version: 10.13,
      Id: 905,
      Name: '7S',
      Latitude: -34.46988688,
      Longitude: 148.69599067,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4334ff59087391'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6959906687845,
        -34.46988688039168
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:56:01',
      Version: 10.13,
      Id: 905,
      Name: '7N',
      Latitude: -34.46963889,
      Longitude: 148.69612073,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433521532c826e'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6961207344677,
        -34.46963888741384
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:58:02',
      Version: 10.13,
      Id: 905,
      Name: '8S',
      Latitude: -34.46989461,
      Longitude: 148.69601253,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43359a7fc111fe'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6960125271466,
        -34.46989461101921
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '11:58:35',
      Version: 10.13,
      Id: 905,
      Name: '8N',
      Latitude: -34.46964677,
      Longitude: 148.69614266,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4335bb1ad6f758'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6961426596417,
        -34.46964677076395
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:00:44',
      Version: 10.13,
      Id: 905,
      Name: '10S',
      Latitude: -34.46991053,
      Longitude: 148.69605657,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43363c0a1afdf5'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69605657449586,
        -34.46991052842593
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:01:18',
      Version: 10.13,
      Id: 905,
      Name: '10N',
      Latitude: -34.46966252,
      Longitude: 148.6961868,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43365e0f15ea76'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69618679579798,
        -34.46966252371161
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:03:35',
      Version: 10.13,
      Id: 905,
      Name: '11S',
      Latitude: -34.46991824,
      Longitude: 148.69607832,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336e75211fd50'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69607831823404,
        -34.469918244828385
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:04:09',
      Version: 10.13,
      Id: 905,
      Name: '11N',
      Latitude: -34.46967043,
      Longitude: 148.69620875,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4337091cb35527'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69620875275524,
        -34.46967043166412
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:06:24',
      Version: 10.13,
      Id: 905,
      Name: '13S',
      Latitude: -34.46993413,
      Longitude: 148.69612248,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43379021594cfc'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69612247830284,
        -34.469934128296586
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:06:58',
      Version: 10.13,
      Id: 905,
      Name: '13N',
      Latitude: -34.46968609,
      Longitude: 148.69625254,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4337b208ea03af'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69625253546315,
        -34.46968608783075
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:10:48',
      Version: 10.13,
      Id: 905,
      Name: '15S',
      Latitude: -34.46995002,
      Longitude: 148.69616696,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338983220e164'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69616695839156,
        -34.46995001896229
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:11:22',
      Version: 10.13,
      Id: 905,
      Name: '15N',
      Latitude: -34.46970184,
      Longitude: 148.6962964,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338ba203fc129'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69629639544485,
        -34.46970183728098
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:14:09',
      Version: 10.13,
      Id: 905,
      Name: '16S',
      Latitude: -34.46995786,
      Longitude: 148.69618864,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43396149faca4f'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69618863965835,
        -34.46995785529968
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:39:14',
      Version: 10.13,
      Id: 905,
      Name: '19S',
      Latitude: -34.46998144,
      Longitude: 148.69625474,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433f4205d25a9c'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69625473552168,
        -34.46998143993471
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:39:49',
      Version: 10.13,
      Id: 905,
      Name: '19N',
      Latitude: -34.46973329,
      Longitude: 148.69638456,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433f655868a4d7'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69638456337495,
        -34.46973328599164
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:44:22',
      Version: 10.13,
      Id: 905,
      Name: '16N',
      Latitude: -34.46970968,
      Longitude: 148.69631848,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4330767fdab577'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69631848339566,
        -34.46970968160135
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:47:13',
      Version: 10.13,
      Id: 905,
      Name: '18S',
      Latitude: -34.46997338,
      Longitude: 148.69623195,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4331210164ed9e'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69623195112993,
        -34.46997337541815
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:47:47',
      Version: 10.13,
      Id: 905,
      Name: '18N',
      Latitude: -34.4697256,
      Longitude: 148.69636279,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4331434183b2ee'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69636279192338,
        -34.46972559863618
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:51:08',
      Version: 10.13,
      Id: 905,
      Name: '21S',
      Latitude: -34.46999705,
      Longitude: 148.69629829,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43320c1b1d68c4'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69629829352078,
        -34.469997048540115
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:51:43',
      Version: 10.13,
      Id: 905,
      Name: '21N',
      Latitude: -34.46974908,
      Longitude: 148.69642838,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43322f5a0dfab1'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69642838138216,
        -34.46974908409683
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:54:31',
      Version: 10.13,
      Id: 905,
      Name: '22S',
      Latitude: -34.47000495,
      Longitude: 148.69632015,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332d76bb93882'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69632015215487,
        -34.47000495107541
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:55:06',
      Version: 10.13,
      Id: 905,
      Name: '22N',
      Latitude: -34.46975692,
      Longitude: 148.69645044,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332fa6fa8b162'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6964504449799,
        -34.469756924442066
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:57:08',
      Version: 10.13,
      Id: 905,
      Name: '23S',
      Latitude: -34.47001285,
      Longitude: 148.69634226,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4333741021b13f'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69634225551587,
        -34.47001285131625
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '12:57:43',
      Version: 10.13,
      Id: 905,
      Name: '23N',
      Latitude: -34.46976466,
      Longitude: 148.69647232,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4333972a59de9a'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69647232487426,
        -34.46976466373167
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:00:42',
      Version: 10.13,
      Id: 905,
      Name: '0S',
      Latitude: -34.46983194,
      Longitude: 148.69583711,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43344a3bb9eaaf'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69583711060258,
        -34.46983194341893
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:01:17',
      Version: 10.13,
      Id: 905,
      Name: '0N',
      Latitude: -34.46958374,
      Longitude: 148.69596667,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43346d450a043e'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69596667031553,
        -34.4695837364752
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:03:55',
      Version: 10.13,
      Id: 905,
      Name: '2S',
      Latitude: -34.469848,
      Longitude: 148.69588193,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43350b08b2da22'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6958819313458,
        -34.46984799772617
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:04:29',
      Version: 10.13,
      Id: 905,
      Name: '2N',
      Latitude: -34.46959947,
      Longitude: 148.69601059,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43352d09ce75a1'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69601058536358,
        -34.46959946624483
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:06:37',
      Version: 10.13,
      Id: 905,
      Name: '5S',
      Latitude: -34.46987117,
      Longitude: 148.69594656,
      Height: 527.002,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4335ad5e17f1f6'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6959465634076,
        -34.46987117038425
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:07:12',
      Version: 10.13,
      Id: 905,
      Name: '5N',
      Latitude: -34.46962317,
      Longitude: 148.69607673,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4335d04bea3f4f'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6960767271412,
        -34.46962317077959
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:09:25',
      Version: 10.13,
      Id: 905,
      Name: '9S',
      Latitude: -34.46990253,
      Longitude: 148.69603423,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336554979daf1'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69603423010963,
        -34.46990252982673
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:09:59',
      Version: 10.13,
      Id: 905,
      Name: '9N',
      Latitude: -34.46965462,
      Longitude: 148.6961646,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: 43367714727410
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6961645986911,
        -34.469654620405564
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:12:08',
      Version: 10.13,
      Id: 905,
      Name: '12S',
      Latitude: -34.46992608,
      Longitude: 148.6961003,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336f818b2a0e1'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69610029572283,
        -34.46992607832404
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:16:08',
      Version: 10.13,
      Id: 905,
      Name: '12N',
      Latitude: -34.46967836,
      Longitude: 148.6962309,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4337e80ba49e9e'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69623090429457,
        -34.469678358107714
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:19:12',
      Version: 10.13,
      Id: 905,
      Name: '14S',
      Latitude: -34.469942,
      Longitude: 148.69614454,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338a03fb05e82'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69614454083418,
        -34.469942002229295
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:19:47',
      Version: 10.13,
      Id: 905,
      Name: '14N',
      Latitude: -34.46969399,
      Longitude: 148.69627472,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338c3064b1d36'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6962747168833,
        -34.46969399058931
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:24:20',
      Version: 10.13,
      Id: 905,
      Name: '17S',
      Latitude: -34.46996571,
      Longitude: 148.69621086,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4339d444f200dd'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69621085757043,
        -34.46996570881537
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:24:54',
      Version: 10.13,
      Id: 905,
      Name: '17N',
      Latitude: -34.46971753,
      Longitude: 148.69634032,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4339f6225d1f9c'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69634032136614,
        -34.469717534281436
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:27:11',
      Version: 10.13,
      Id: 905,
      Name: '20S',
      Latitude: -34.46998922,
      Longitude: 148.69627619,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433a7f2a7bc7ea'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69627618896453,
        -34.469989221232574
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-03-18',
      Time: '01:27:46',
      Version: 10.13,
      Id: 905,
      Name: '20N',
      Latitude: -34.46974125,
      Longitude: 148.69640646,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433aa24fe6fef8'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69640646260382,
        -34.469741251735336
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:45:08',
      Version: 10.13,
      Id: 905,
      Name: '24S',
      Latitude: -34.47002088,
      Longitude: 148.69636622,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332a400d71938'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69636622128283,
        -34.4700208814247
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:45:46',
      Version: 10.13,
      Id: 905,
      Name: '24N',
      Latitude: -34.46977274,
      Longitude: 148.69649598,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4332ca401fbd3c'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69649597593806,
        -34.46977274022107
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:48:24',
      Version: 10.13,
      Id: 905,
      Name: '25S',
      Latitude: -34.47002942,
      Longitude: 148.69639025,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4333684af70895'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69639025335286,
        -34.470029424215596
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:49:02',
      Version: 10.13,
      Id: 905,
      Name: '25N',
      Latitude: -34.4697813,
      Longitude: 148.69652,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43338e6042d9b7'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69651999817864,
        -34.46978130370841
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:52:21',
      Version: 10.13,
      Id: 905,
      Name: '29S',
      Latitude: -34.47006377,
      Longitude: 148.69648599,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43345525bcb1c5'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69648599118722,
        -34.47006376949785
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:52:59',
      Version: 10.13,
      Id: 905,
      Name: '29N',
      Latitude: -34.46981569,
      Longitude: 148.69661594,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43347b37e76646'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6966159353137,
        -34.469815685620915
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:54:51',
      Version: 10.13,
      Id: 905,
      Name: '31S',
      Latitude: -34.47008101,
      Longitude: 148.69653434,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4334eb259257fb'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6965343418727,
        -34.470081012596566
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:55:29',
      Version: 10.13,
      Id: 905,
      Name: '31N',
      Latitude: -34.46983288,
      Longitude: 148.6966639,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433511446593ce'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6966638996377,
        -34.469832884933695
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:57:23',
      Version: 10.13,
      Id: 905,
      Name: '34S',
      Latitude: -34.47010675,
      Longitude: 148.69660621,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433583318e858d'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69660620709882,
        -34.470106745817915
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '12:58:01',
      Version: 10.13,
      Id: 905,
      Name: '34N',
      Latitude: -34.46985861,
      Longitude: 148.69673584,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4335a917a984a3'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69673584460503,
        -34.46985861179975
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:00:03',
      Version: 10.13,
      Id: 905,
      Name: '38S',
      Latitude: -34.47014105,
      Longitude: 148.69670197,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433623001c071f'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69670196822256,
        -34.47014104969544
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:00:41',
      Version: 10.13,
      Id: 905,
      Name: '38N',
      Latitude: -34.46989291,
      Longitude: 148.69683168,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336495010e305'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69683168075025,
        -34.46989291267806
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:02:26',
      Version: 10.13,
      Id: 905,
      Name: '40S',
      Latitude: -34.47015829,
      Longitude: 148.6967499,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336b26b97351d'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69674989714534,
        -34.47015829371922
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:03:04',
      Version: 10.13,
      Id: 905,
      Name: '40N',
      Latitude: -34.46991008,
      Longitude: 148.69687969,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4336d86be4f760'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69687968905413,
        -34.46991008433271
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:05:09',
      Version: 10.13,
      Id: 905,
      Name: '44S',
      Latitude: -34.47019255,
      Longitude: 148.69684564,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: 43375544125244
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69684564398398,
        -34.470192548487255
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:05:47',
      Version: 10.13,
      Id: 905,
      Name: '44N',
      Latitude: -34.46994444,
      Longitude: 148.69697565,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43377b71070dc2'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69697564854067,
        -34.46994443853405
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:10:25',
      Version: 10.13,
      Id: 905,
      Name: '26S',
      Latitude: -34.47003805,
      Longitude: 148.69641425,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338915d19404e'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69641424783026,
        -34.4700380531759
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:11:00',
      Version: 10.13,
      Id: 905,
      Name: '26N',
      Latitude: -34.46978998,
      Longitude: 148.69654414,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4338b418b3adb6'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6965441373404,
        -34.46978998197102
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:12:54',
      Version: 10.13,
      Id: 905,
      Name: '28S',
      Latitude: -34.47005528,
      Longitude: 148.69646242,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4339261da9a080'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69646242284844,
        -34.47005527970558
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:13:30',
      Version: 10.13,
      Id: 905,
      Name: '28N',
      Latitude: -34.46980709,
      Longitude: 148.69659191,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '43394a57e7b8fc'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6965919131796,
        -34.469807091782755
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:15:30',
      Version: 10.13,
      Id: 905,
      Name: '32S',
      Latitude: -34.47008965,
      Longitude: 148.69655829,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4339c2202946d9'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69655829096325,
        -34.47008964592281
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:16:06',
      Version: 10.13,
      Id: 905,
      Name: '32N',
      Latitude: -34.46984132,
      Longitude: 148.69668773,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4339e67b311da7'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69668773380846,
        -34.46984132238838
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:18:11',
      Version: 10.13,
      Id: 905,
      Name: '35S',
      Latitude: -34.47011539,
      Longitude: 148.69663026,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433a635f4bb442'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69663026498264,
        -34.470115386242114
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:18:47',
      Version: 10.13,
      Id: 905,
      Name: '35N',
      Latitude: -34.46986717,
      Longitude: 148.69675971,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433a8713bcfd62'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69675970763848,
        -34.46986716751396
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:20:32',
      Version: 10.13,
      Id: 905,
      Name: '37S',
      Latitude: -34.4701326,
      Longitude: 148.69667813,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433af057f2ddac'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6966781328703,
        -34.47013259733447
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:21:08',
      Version: 10.13,
      Id: 905,
      Name: '37N',
      Latitude: -34.46988435,
      Longitude: 148.69680786,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433b14158d94ca'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69680785949652,
        -34.469884353039916
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:23:07',
      Version: 10.13,
      Id: 905,
      Name: '41S',
      Latitude: -34.47016681,
      Longitude: 148.69677393,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433b8b4d0a7b92'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69677393479685,
        -34.470166809627386
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:23:43',
      Version: 10.13,
      Id: 905,
      Name: '41N',
      Latitude: -34.46991876,
      Longitude: 148.69690379,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433baf7dd39160'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69690378584482,
        -34.469918763350435
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:25:31',
      Version: 10.13,
      Id: 905,
      Name: '43S',
      Latitude: -34.470184,
      Longitude: 148.69682179,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433c1b245d180f'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6968217874891,
        -34.47018399853665
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:26:07',
      Version: 10.13,
      Id: 905,
      Name: '43N',
      Latitude: -34.46993579,
      Longitude: 148.69695167,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433c3f3a8674a8'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69695166973403,
        -34.46993578639703
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:30:16',
      Version: 10.13,
      Id: 905,
      Name: '27S',
      Latitude: -34.47004665,
      Longitude: 148.69643822,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433d38068e9aa6'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6964382214171,
        -34.470046652892016
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:30:52',
      Version: 10.13,
      Id: 905,
      Name: '27N',
      Latitude: -34.46979849,
      Longitude: 148.69656799,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433d5c4e73a2a0'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69656798683638,
        -34.469798485220124
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:32:37',
      Version: 10.13,
      Id: 905,
      Name: '30S',
      Latitude: -34.47007252,
      Longitude: 148.69651059,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433dc568be293a'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.6965105919685,
        -34.470072522455126
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:33:12',
      Version: 10.13,
      Id: 905,
      Name: '30N',
      Latitude: -34.4698242,
      Longitude: 148.69663979,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433de855536bd7'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69663979015817,
        -34.46982420231346
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:35:11',
      Version: 10.13,
      Id: 905,
      Name: '33S',
      Latitude: -34.47009824,
      Longitude: 148.69658219,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433e5f28c00822'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69658218617874,
        -34.47009823720901
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:35:47',
      Version: 10.13,
      Id: 905,
      Name: '33N',
      Latitude: -34.46984999,
      Longitude: 148.69671182,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433e830c38427c'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69671181775487,
        -34.469849990636824
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:37:38',
      Version: 10.13,
      Id: 905,
      Name: '36S',
      Latitude: -34.47012393,
      Longitude: 148.69665394,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433ef242ed3654'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69665393979605,
        -34.470123931540165
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:38:13',
      Version: 10.13,
      Id: 905,
      Name: '36N',
      Latitude: -34.46987573,
      Longitude: 148.69678359,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433f1537b479a5'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69678358730818,
        -34.46987573060628
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:40:03',
      Version: 10.13,
      Id: 905,
      Name: '39S',
      Latitude: -34.47014977,
      Longitude: 148.69672634,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433f8301c461e6'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69672634101502,
        -34.47014976951934
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:40:39',
      Version: 10.13,
      Id: 905,
      Name: '39N',
      Latitude: -34.46990151,
      Longitude: 148.69685577,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433fa7624688b8'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69685576579317,
        -34.46990150865274
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:42:24',
      Version: 10.13,
      Id: 905,
      Name: '42S',
      Latitude: -34.47017542,
      Longitude: 148.69679789,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433010609e8cf7'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69679788570392,
        -34.470175418604605
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:43:00',
      Version: 10.13,
      Id: 905,
      Name: '42N',
      Latitude: -34.46992729,
      Longitude: 148.69692766,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '433034767c3de8'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69692765787815,
        -34.46992728619955
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:44:48',
      Version: 10.13,
      Id: 905,
      Name: '45S',
      Latitude: -34.47020126,
      Longitude: 148.69686984,
      Height: 527.003,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4330a0435b01c4'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69686983577773,
        -34.47020125533992
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      markerColor: '#7e7e7e',
      markerSize: 'medium',
      markerSymbol: '',
      Date: '2021-06-02',
      Time: '01:45:23',
      Version: 10.13,
      Id: 905,
      Name: '45N',
      Latitude: -34.46995305,
      Longitude: 148.69699957,
      Height: 527.004,
      AlarmRad: 0,
      WarningRad: 0,
      status: 'null',
      Visible: 1,
      UniqueID: '4330c342df0f69'
    },
    geometry: {
      type: 'Point',
      coordinates: [
        148.69699957425462,
        -34.46995305397497
      ]
    }
  }
];

module.exports = plotEndPoints;
