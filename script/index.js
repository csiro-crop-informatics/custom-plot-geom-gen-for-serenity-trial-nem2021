const {
  rhumbBearing,
  buildPlotFromEndPoints,
  buildSectionsFromEndPoints,
  smallestBoundingRect,
  featureCollection,
} = require('./geomFunctions');
const plotEndPointsGeoJSON = require('../data/plotEndPointsGeoJSON');

const numPlotRuns = 46;
const trial1Start = 1;
const trial1End = 22;
const trial2Start = 24;
const trial2End = 45;
const plotWidthKm = 0.0021;
const section1LengthKm = 0.004;

const generateGeomData = () => {
  const keyedPlotEnds = keyPointsByName(plotEndPointsGeoJSON);
  const plotBounds = buildPlotBounds(keyedPlotEnds);
  const trial1Bounds = smallestBoundingRect(
    featureCollection([plotBounds[trial1Start], plotBounds[trial1End]]),
  );
  const trial2Bounds = smallestBoundingRect(
    featureCollection([plotBounds[trial2Start], plotBounds[trial2End]]),
  );
  return [trial1Bounds, trial2Bounds, stringifyPlotBounds(plotBounds)];
};

const keyPointsByName = (pointsGeoJSON) => {
  return pointsGeoJSON.reduce((result, point) => {
    result[point.properties.Name] = point;
    return result;
  }, {});
};

const buildPlotBounds = (keyedPlotEnds) => {
  let plotBounds = [];
  for (let i = 0; i < numPlotRuns; i++) {
    const nPoint = keyedPlotEnds[`${i}N`];
    const sPoint = keyedPlotEnds[`${i}S`];
    const bearing = rhumbBearing(sPoint, nPoint);
    if (i >= 2 && i <= 21) {
      // Build section-split plots
      plotBounds.push(
        buildSectionsFromEndPoints(
          sPoint,
          nPoint,
          bearing,
          plotWidthKm,
          section1LengthKm,
        ),
      );
    } else {
      // Build single plot
      plotBounds.push(
        buildPlotFromEndPoints(sPoint, nPoint, bearing, plotWidthKm),
      );
    }
  }
  return plotBounds;
};

const stringifyPlotBounds = (plotBounds) => {
  let result = 'run,section,geoJSON\n';
  for (let i = 1; i < numPlotRuns; i++) {
    if (i >= 2 && i <= 21) {
      result += `${i},1,"` + stringify1Plot(plotBounds[i][0]) + '"\n';
      result += `${i},2,"` + stringify1Plot(plotBounds[i][1]) + '"\n';
    } else {
      result += `${i},2,"` + stringify1Plot(plotBounds[i]) + '"\n';
    }
  }
  return result;
};

const stringify1Plot = (plotBound) => {
  return JSON.stringify(plotBound.geometry).replace(/"/g, '""');
};

module.exports = generateGeomData;
