const bearing = require('@turf/bearing').default;
const rhumbBearing = require('@turf/rhumb-bearing').default;
const rhumbDestination = require('@turf/rhumb-destination').default;
const convex = require('@turf/convex').default;
const centroid = require('@turf/centroid').default;
const transformRotate = require('@turf/transform-rotate').default;
const envelope = require('@turf/envelope').default;
const area = require('@turf/area').default;
const { coordAll } = require('@turf/meta');
const { featureCollection } = require('@turf/helpers');

/** Modified from https://github.com/matthiasfeist/geojson-minimum-bounding-rectangle
 */
const smallestBoundingRect = (geoJsonInput) => {
  const convexHull = convex(geoJsonInput);
  const centroidCoords = centroid(convexHull);
  const allHullCoords = coordAll(convexHull);
  let minArea = Number.MAX_SAFE_INTEGER;
  let resultPolygon = null;
  for (let index = 0; index < allHullCoords.length - 1; index++) {
    let angle = bearing(allHullCoords[index], allHullCoords[index + 1]);
    let rotatedHull = transformRotate(convexHull, -1.0 * angle, {
      pivot: centroidCoords,
    });
    let envelopeOfHull = envelope(rotatedHull);
    let envelopeArea = area(envelopeOfHull);
    if (envelopeArea < minArea) {
      minArea = envelopeArea;
      resultPolygon = transformRotate(envelopeOfHull, angle, {
        pivot: centroidCoords,
      });
    }
  }
  return resultPolygon.geometry;
};

const normaliseBearing = (bearing) => {
  if (bearing < -180) {
    return bearing + 360;
  }
  if (bearing > 180) {
    return bearing - 360;
  }
  return bearing;
};

const buildPlotFromEndPoints = (sPoint, nPoint, bearing, plotWidthKm) => {
  const halfWidthKm = plotWidthKm / 2.0;
  const unitsOpt = {
    units: 'kilometers',
  };
  let bearLeft = normaliseBearing(bearing - 90);
  let bearRight = normaliseBearing(bearing + 90);
  // Corner to left of south point
  const pS1 = rhumbDestination(sPoint, halfWidthKm, bearLeft, unitsOpt);
  // Corner to right of south point
  const pS2 = rhumbDestination(sPoint, halfWidthKm, bearRight, unitsOpt);
  // Corner to left of north point
  const pN1 = rhumbDestination(nPoint, halfWidthKm, bearLeft, unitsOpt);
  // Corner to right of north point
  const pN2 = rhumbDestination(nPoint, halfWidthKm, bearRight, unitsOpt);
  // Build points into polygon / rectangle
  let polygon = convex(featureCollection([pS1, pS2, pN1, pN2]));
  return polygon;
};

const buildSectionsFromEndPoints = (
  sPoint,
  nPoint,
  bearing,
  plotWidthKm,
  section1LengthKm,
) => {
  const halfWidthKm = plotWidthKm / 2.0;
  const unitsOpt = {
    units: 'kilometers',
  };
  let bearLeft = normaliseBearing(bearing - 90);
  let bearRight = normaliseBearing(bearing + 90);
  // Corner to left of south point
  const pS1 = rhumbDestination(sPoint, halfWidthKm, bearLeft, unitsOpt);
  // Corner to right of south point
  const pS2 = rhumbDestination(sPoint, halfWidthKm, bearRight, unitsOpt);
  // Corner to left of north point
  const pN1 = rhumbDestination(nPoint, halfWidthKm, bearLeft, unitsOpt);
  // Corner to right of north point
  const pN2 = rhumbDestination(nPoint, halfWidthKm, bearRight, unitsOpt);
  // Sectioning corners along plot
  const pM1 = rhumbDestination(pS1, section1LengthKm, bearing, unitsOpt);
  const pM2 = rhumbDestination(pS2, section1LengthKm, bearing, unitsOpt);
  // Build points into polygons
  let sect1Poly = convex(featureCollection([pS1, pS2, pM1, pM2]));
  let sect2Poly = convex(featureCollection([pN1, pN2, pM1, pM2]));
  return [sect1Poly, sect2Poly];
};

module.exports = {
  smallestBoundingRect,
  rhumbBearing,
  normaliseBearing,
  buildPlotFromEndPoints,
  buildSectionsFromEndPoints,
  featureCollection,
};
